FROM ubuntu

RUN apt update -y
RUN apt install -y proftpd acl

ADD proftpd.conf /etc/proftpd/proftpd.conf
ADD passwd /etc/proftpd/auth/passwd
RUN chmod o-rwx /etc/proftpd/auth/passwd
RUN chown root:root /etc/proftpd/auth/passwd
RUN groupadd ftp
RUN chown root:root /etc/proftpd/proftpd.conf
RUN mkdir /ftp

EXPOSE 21
EXPOSE 20
EXPOSE 50000-50005

RUN useradd -s /bin/bash -d /ftp defaultuser
RUN usermod -G defaultuser defaultuser
RUN echo 'defaultuser:defaultuser' | chpasswd
RUN usermod -G users defaultuser
RUN chown defaultuser:users /ftp -R
RUN setfacl -m u:defaultuser:rwx /ftp
RUN setfacl -m d:defaultuser:rwx /ftp

CMD ["proftpd", "--nodaemon", "-c", "/etc/proftpd/proftpd.conf"]