Create the container with the following command:

    sudo docker run -dit -m 256mb --name proftpd -h proftpd -v /ftp:/ftp -p 20:20 -p 21:21 -p 50000-50005:50000-50005 linuxmanco/proftpd

Transfers on your server will be located in `/ftp`

Default username is `defaultuser` and password `defaultuser`

If hosting publicly, remember to open the ports on your server.